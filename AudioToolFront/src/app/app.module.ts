import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DragAndDropUploadComponent } from './drag-and-drop-upload/drag-and-drop-upload.component';
import { WaveformEditorComponent } from './waveform-editor/waveform-editor.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NgxUploaderDirective } from './ngx-uploader-directive/ngx-uploader.directive';
import {NgFileSelectDirective} from "./ngx-uploader-directive/ngx-uploader-select.directive";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

@NgModule({
  declarations: [
    AppComponent,
    DragAndDropUploadComponent,
    WaveformEditorComponent,
    NavigationComponent,
    NgxUploaderDirective,
    NgFileSelectDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
