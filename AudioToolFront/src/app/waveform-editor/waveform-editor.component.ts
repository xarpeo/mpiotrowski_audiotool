import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UploadInput} from 'ngx-uploader';
import {Track} from '../model/Track';
import {forEach} from '@angular/router/src/utils/collection';
import * as d3 from 'd3';

@Component({
  selector: 'app-waveform-editor',
  templateUrl: './waveform-editor.component.html',
  styleUrls: ['./waveform-editor.component.css']
})
export class WaveformEditorComponent implements OnInit {

  track: Track;
  audioContext = new AudioContext();
  lineDataFromAudioBuffer = Array<{x1: Number, y1: Number, x2: Number, y2: Number}>();
  SCALE = 300;

  analyzeFile($event) {

    const self = this;
    const fileReader = new FileReader();
    fileReader.readAsArrayBuffer($event.file.nativeFile);
    fileReader.onloadend = function (event) {
      self.track.trackArrayBuffer = event.target.result;
      // debugger;

      self.audioContext.decodeAudioData(self.track.trackArrayBuffer)
        .then(function (audioBuffer) {
          self.fillLineData(audioBuffer);

        });
    };

  }

  ngOnInit() {
    this.track = new Track();
  }

  fillLineData(decodingResult: AudioBuffer) {

    // var myCanvas = document.querySelector('waveformPlaceholder');
    const HEIGHT = 160;
    const WIDTH = 500;
    const canvas = d3.select('#waveformPlaceholder').append('canvas')
      .attr('height', HEIGHT)
      .attr('width', WIDTH);
    const context = (canvas.node() as HTMLCanvasElement).getContext('2d');
    const leftChannelSamples = decodingResult.getChannelData(0);

    context.fillStyle = '#ccddff';
    context.fillRect(0, 0, WIDTH, HEIGHT);
    context.moveTo(0, HEIGHT / 2 );
    context.lineTo(WIDTH, HEIGHT / 2);
    context.stroke();
    context.fillStyle = '#ff003e';
    context.beginPath();
    // prepareLineFunction(leftChannelSamples, context);
    const customBase = document.createElement('custom');
    const custom = d3.select(customBase);
    custom.selectAll('custom.line')
      .datum(leftChannelSamples);

    custom.select('custom').datum(leftChannelSamples)
    .enter()
      .attr('y1', function(d) { return d[0]; })
      .attr('y2', function(d) { return d[1]; })
      .attr('x1', function(d, i) { return i + 0.5; })
      .attr('x2', function(d, i) { return i + 0.5; })
      .attr('stroke-width', 1)
      .attr('stroke', 'green')
    context.stroke();
      // .attr('d', leftChannelSamples.slice());
    // context.lineTo()


  }

  private prepareLineFunction(leftChannelSamples: Float32Array, context) {
    return d3.line()
      .x((d, i) => i)
      .y((d, i) => d[i])
      .curve(d3.curveStep)
      .context(context);
  }

    // var enterSel = join.enter()
    //   .append('custom')
    //   .attr('class', 'line')
    //   .attr("x1", function(d, i) {
    //     var x0 = Math.floor(i / 100) % 10, x1 = Math.floor(i % 10);
    //     return 1; //groupSpacing * x0 + (cellSpacing + cellSize) * (x1 + x0 * 10);
    //   })
    //   .attr("y", function(d, i) {
    //     var y0 = Math.floor(i / 1000), y1 = Math.floor(i % 100 / 10);
    //     return 2; // groupSpacing * y0 + (cellSpacing + cellSize) * (y1 + y0 * 10);
    //   })
    //   .attr('width', 0)
    //   .attr('height', 0);
  // }

}
