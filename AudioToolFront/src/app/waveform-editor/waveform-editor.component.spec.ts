import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaveformEditorComponent } from './waveform-editor.component';

describe('WaveformEditorComponent', () => {
  let component: WaveformEditorComponent;
  let fixture: ComponentFixture<WaveformEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaveformEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaveformEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
