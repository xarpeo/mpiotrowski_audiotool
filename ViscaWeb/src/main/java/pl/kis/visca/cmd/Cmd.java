package pl.kis.visca.cmd;

/**
 * Created by Michał Piotrowski on 2018-11-15.
 */
public abstract class Cmd {
    public Cmd() {
    }

    public abstract byte[] createCommandData();
}
