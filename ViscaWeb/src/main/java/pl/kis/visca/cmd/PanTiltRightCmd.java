package pl.kis.visca.cmd;

/**
 * Created by Michał Piotrowski on 2018-11-15.
 */
public final class PanTiltRightCmd extends Cmd {
    private static final byte[] ptRightCommandData = new byte[]{1, 6, 1, 0, 0, 2, 3};

    public PanTiltRightCmd() {
    }

    public byte[] createCommandData() {
        byte[] cmdData = duplicatArray(ptRightCommandData);
        cmdData[3] = 4;
        cmdData[4] = 1;
        return cmdData;
    }

    private static byte[] duplicatArray(byte[] src) {
        byte[] dest = new byte[src.length];
        System.arraycopy(src, 0, dest, 0, src.length);
        return dest;
    }
}
