package bean;

import jssc.SerialPort;
import jssc.SerialPortException;
import pl.kis.visca.Main;
import pl.kis.visca.ViscaResponseReader;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by Michał Piotrowski on 2018-12-05.
 */
//@Named
@SessionScoped
@ManagedBean
public class ViscaBackingClass implements Serializable {

    public Main m;
    private String port = null;
    private SerialPort serialPort = null;

    private String cameraResponse = "Camera Response";

    public ViscaBackingClass() throws IOException, SerialPortException {
        this.port = "COM3";
        SerialPort serialPort = new SerialPort(this.port);
//        serialPort.openPort();

//        serialPort.setParams(9600, 8, 1, 0);
        m = new Main();
        FacesContext.getCurrentInstance();
        this.cameraResponse = "New Visca Class";
    }

    public String getCameraResponse() {
        System.out.println("camera response");
        cameraResponse = "CAMERA RESPONSE";
        byte[] response;
//        try {
//            response = ViscaResponseReader.readResponse(serialPort);
//            System.out.println("> " + byteArrayToString(response));
//        } catch (ViscaResponseReader.TimeoutException var17) {
//            System.out.println("! TIMEOUT exception");
//        } catch (SerialPortException e) {
//            e.printStackTrace();
//        }
        return cameraResponse;
    }

    private static String byteArrayToString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        byte[] var5 = bytes;
        int var4 = bytes.length;

        for (int var3 = 0; var3 < var4; ++var3) {
            byte b = var5[var3];
            sb.append(String.format("%02X ", b));
        }

        return sb.toString();
    }

    public void setCameraResponse(String cameraResponse) {
        System.out.println("set camera response");
        this.cameraResponse = cameraResponse;
    }

    public void up() throws SerialPortException {
//        System.out.println("set camera response");
//        MainExt.executeCommand("Up", new SerialPort("COM3"));
//        this.cameraResponse = cameraResponse;
        Main.executeCommand("sendPanTiltUp", serialPort);
    }

    public Main getM() {
        return m;
    }

    public void setM(Main m) {
        this.m = m;
    }

    public boolean getInitialized() {
        return this.m == null;
    }



    public void down() {
        System.out.println("down");

    }

    public void right() {
        System.out.println("right");

    }

    public void left() {
        System.out.println("left");

    }

    public void home() {
        System.out.println("home");

    }
}
