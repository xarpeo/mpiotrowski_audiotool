package com.piotrowski.audiotoolback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AudioToolBackApplication {

  public static void main(String[] args) {
    SpringApplication.run(AudioToolBackApplication.class, args);
  }
}
